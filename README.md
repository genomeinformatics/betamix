# README #

Clone this repository into a local directory and inspect it.

All code and data is contained herein.
To reproduce everything from scratch, follow the steps described in workflow.txt

In case of questions, contact Sven.Rahmann [-@-] uni-due [-.-] de

Have fun!