from itertools import count
from argparse import ArgumentParser
import os
import os.path

import numpy as np
from numba import njit
from scipy.stats import beta
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
from h5py import File as H5File


def ensure_directory(d):
    """
    Ensure that directory <d> exists.
    It is an error to pass anything else than a directory string.
    """
    if d == "": return
    d = os.path.abspath(d)
    os.makedirs(d, exist_ok=True)


def get_argument_parser():
    p = ArgumentParser(description="Summarization of the evaluation of beta mixtures")
    p.add_argument("--imagepath", "-p", "-i", metavar="IMAGEPATH",
        help="path to save images to (omit for no images)")
    p.add_argument("--format", "-f", default="pdf",
        help="output format for images [pdf]")
    p.add_argument("--dpi", "-D", type=int, default=200,
        help="dpi for output images")
    p.add_argument("--bins", "-B", type=int,
        help="number of bins for plotting histograms")
    p.add_argument("evalpath", metavar="EVAL_HDF5",
        help="path to HDF5 file with evaluation") 
    return p


def main():
    p = get_argument_parser()
    args = p.parse_args()
    ipath = args.imagepath
    if ipath is not None:
        ensure_directory(ipath)
        sns.set(font_scale=2)
        sns.set_style("whitegrid")
        
    with  H5File(args.evalpath, "r") as feval:
        geval = feval['evaluation']
        ds = list(geval.keys())
        N = len(ds)
        abcs = np.zeros(len(ds), dtype=float)
        dbcs = np.zeros(len(ds), dtype=float)
        for i, d in enumerate(ds):
            abcs[i] = geval[d+'/abc'][()]
            dbcs[i] = geval[d+'/dbc'][()]
    # output statistics
    ZERO = 1E-6
    ap = np.sum(abcs > ZERO)
    an = np.sum(abcs < -ZERO)
    az = N - (ap+an)
    am = np.mean(abcs)
    dp = np.sum(dbcs > ZERO)
    dn = np.sum(dbcs < -ZERO)
    dz = N - (dp+dn)
    dm = np.mean(dbcs)
    print("areas:     + = {};  - = {};  0 = {};  mean = {:.4f}".format(ap,an,az,am))
    print("distances: + = {};  - = {};  0 = {};  mean = {:.4f}".format(dp,dn,dz,dm))
    # plot histogram of areas between curves (ABCs)
    bins = args.bins if args.bins is not None else N//20
    if ipath is not None:
        outpath = ipath + '/areas.' + args.format
        sns.distplot(abcs, kde=False, bins=bins, norm_hist=True)
        plt.xlabel('area between curves')
        plt.ylabel('frequency')
        plt.savefig(outpath, bbox_inches='tight', dpi=args.dpi)
        plt.close()
        outpath = ipath + '/distances.' + args.format
        sns.distplot(dbcs, kde=False, bins=bins, norm_hist=True)
        plt.xlabel('distance between curves at full classification')
        plt.ylabel('frequency')
        plt.savefig(outpath, bbox_inches='tight', dpi=args.dpi)
        plt.close()


if __name__ == "__main__":
    main()

