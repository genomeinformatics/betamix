# betamix workflow
# run these steps to reproduce the results of the paper

# Note: You need certain packages installed.
# The best option is to use the conda installer,
# create an environment using the conda.env file in this directory: 
#   conda create --name betamix  --file conda.env
# If that does not work, try manually:
#   conda create --name betamix numpy scipy matplotlib seaborn pandas h5py numba pip
# Activate the environment:
#   source activate betamix  # Under Windows: activate betamix
# When you are done running the examples, use
#   source deactivate  # Windows: deactivate

# Information:
# Each python program below can be called with --help to see options.

# simulate 1000 DMRs with correlated SNPs from 100 individuals
python simulate.py -m 1000 -n 200 -0 3 -1 3 --seed 17 data-snps.h5

# simulate 1000 DMRs with correlated SNPs from 1000 individuals
python simulate.py -m 1000 -n 1000 -0 10 -1 10 --seed 39 data-bigsnps.h5

# estimate parameters
python estimate.py -F -t 1E-5  data-snps.h5 est-snps.h5
python estimate.py -F -t 1E-5  data-bigsnps.h5 est-bigsnps.h5

# evaluate simulated results
python evaluate.py -F -i snps -f pdf data-snps.h5 est-snps.h5 eval-snps.h5
python evaluate.py -F -i bigsnps -f pdf data-bigsnps.h5 est-bigsnps.h5 eval-bigsnps.h5

# summarize results
python summarize.py -i snps eval-snps.h5
python summarize.py -i bigsnps eval-snps.h5


